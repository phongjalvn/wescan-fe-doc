const tooltips = {
  ads: {
    items: {
      active: "Sử dụng hình ảnh này",
      animation: {
        in: "Hiệu ứng hiện",
        out: "Hiệu ứng ẩn",
      },
      image: "Hình ảnh",
      name: "Tên quảng cáo, không hiển thị, nhưng bạn nên nhập để dễ quản lý",
      time: "Thời gian hiển thị",
    },
  },
  banner: {
    addon_services: {
      playerduo: {
        delay: "Thời gian delay giữa 2 lần lấy dữ liệu từ Player Duo",
        link: "Copy URL Player Duo cung cấp cho bạn vào đây",
      },
      status: false,
      unghotoi: {
        delay: "Thời gian delay giữa 2 lần lấy dữ liệu từ Unghotoi",
        link: "Copy URL Unghotoi cung cấp cho bạn vào đây",
      },
    },
    animation: {
      background:
        "Hiệu ứng nền, chỉ áp dụng cho các donator đã được xác thực số điện thoại",
      backgroundConfig: {
        speed: "Tốc độ hiệu ứng",
        time: "Thời gian chạy hiệu ứng",
        total: "Số lượng hiệu ứng hiển thị",
      },
      in: "Hiệu ứng xuất hiện và biến mất",
      out: "Hiệu ứng biến mất",
      text:
        "Hiệu ứng của chữ (Chỉ áp dụng cho tên khán giả, số tiền giao dịch và coupon)",
    },
    background:
      "Chọn màu nền và chọn độ mờ. Nếu muốn nền trong suốt thì kéo thanh thứ 2 sát về bên trái nhé.",
    coupon: {
      animation:
        "Hiệu ứng của chữ (Chỉ áp dụng cho tên khán giả, số tiền giao dịch và coupon)",
      enable: "Bật tắt hiển thị coupon",
      font:
        "Chọn font hiển thị, xem danh sách font tại https://fonts.google.com/",
      font_color: "Màu sắc chữ",
      font_highlight: "Màu tên khán giả, số tiền giao dịch và coupon.",
      font_size: "Kích thước font chữ (px), khuyến nghị từ 32-60",
      font_weight:
        "Độ đậm font chữ, lưu ý, nên tham khảo để chắc chắn font bạn đang sử dụng hỗ trợ độ đậm tương ứng. Tham khảo tại https://fonts.google.com/",
      status: false,
      template: "Mẫu hiển thị thông báo coupon.",
      voice: "Bật tắt đọc thông báo coupon",
      voice_template: "Mẫu đọc thông báo coupon.",
    },
    custom_css: {
      css: "",
      status: "Tùy chỉnh css nếu bạn thông thạo món này.",
    },
    delay:
      "Thời gian nghỉ giữa 2 giao dịch. Hãy chỉnh thời gian này cao lên nếu bạn có nhiều khán giả ủng hộ cùng 1 lúc, để không bị gián đoạn việc stream.",
    donator_msg: {
      font:
        "Chọn font hiển thị, xem danh sách font tại https://fonts.google.com/",
      font_color: "Màu sắc chữ",
      font_size: "Kích thước font chữ (px), khuyến nghị từ 32-60",
      font_weight:
        "Độ đậm font chữ, lưu ý, nên tham khảo để chắc chắn font bạn đang sử dụng hỗ trợ độ đậm tương ứng. Tham khảo tại https://fonts.google.com/",
      min_donate:
        "Số tiền tối thiểu để hiện tin nhắn của khán giả. Nên cấu hình cao hơn số tiền tối thiểu cho 1 giao dịch để lọc bớt các giao dịch giá trị thấp.",
      status: true,
    },
    duration:
      "Thời gian hiện giao dịch sau khi Google đọc xong trước khi biến mất",
    font:
      "Chọn font hiển thị, xem danh sách font tại https://fonts.google.com/",
    font_color: "Màu sắc chữ",
    font_highlight: "Màu tên khán giả, số tiền giao dịch và coupon.",
    font_size: "Kích thước font chữ (px), khuyến nghị từ 32-60",
    font_weight:
      "Độ đậm font chữ, lưu ý, nên tham khảo để chắc chắn font bạn đang sử dụng hỗ trợ độ đậm tương ứng. Tham khảo tại https://fonts.google.com/",
    hide_news: "Ẩn thông báo khi có donate",
    image: "static/default.gif",
    layout: "Chọn cách hiển thị hình ảnh và văn bản",
    min_donate:
      "Số tiền nhỏ nhất để hiển thị giao dịch mới lên màn hình stream. Cấu hình thấp thôi nếu bạn là lính mới. Còn đã là cao thủ, có lượng fan hùng hậu rồi thì tăng cao lên nhé.",
    msg_template:
      "Mẫu hiển thị thông báo tên khán giả và số tiền giao dịch. Hãy thể hiện sự biết ơn của bạn tới khán giả bằng những câu cảm ơn chân tình nhất nhé.",
    msg_template_noname:
      "Mẫu hiển thị thông báo khi khán giả chưa đăng ký trên wescan",
    msg_template_notsupport:
      "Mẫu hiển thị thông báo khi khán giả donate từ những ngân hàng không được hỗ trợ hiển thị tên bởi hệ thống wescan",
    sound: "static/default.ogg",
    sound_time:
      "Thời gian từ lúc hiện ra giao dịch đến lúc bắt đầu có Google đọc",
    sound_volume: "Âm lượng của âm thanh, nên để 50-70%",
    total_duration:
      "Tổng thời gian hiện. Thông báo sẽ bị tắt sau khi hết thời gian này dù âm thanh và google đọc chưa kết thúc",
    tts: {
      donator_msg:
        "Tắt nếu không muốn Google đọc tên khán giả và số tiền giao dịch",
      min_donate:
        "Số tiền tối thiểu để có Google đọc. Nên cấu hình cao hơn số tiền tối thiểu cho 1 giao dịch để lọc bớt các giao dịch giá trị thấp.",
      status: "Bật để Google đọc.",
      voice: "Chọn giọng đọc, nên chọn đúng ngôn ngữ bạn đang sử dụng",
      voice_volume: "Âm lượng của Google đọc, nên để 50-70%",
    },
  },
  list: {
    animation: {
      in: "Hiệu ứng xuất hiện",
      out: "Hiệu ứng biến mất",
      text: "Hiệu ứng tên khán giả và số tiền giao dịch",
    },
    append_msg: "Thông báo hiển thị sau khi chạy hết tất cả",
    background:
      "Chọn màu nền và chọn độ mờ. Nếu muốn nền trong suốt thì kéo thanh thứ 2 sát về bên trái nhé.",
    delay: "Thời gian chờ giữa 2 lần chạy",
    display:
      'Kiểu hiển thị, chọn "Tuần tự" để hiện mỗi lần 1 tin mới, chọn "Truyền thống" để hiện tất cả',
    duration:
      "Thời gian hiện trước khi biến mất, nhường chỗ cho giao dịch kế tiếp.",
    font:
      "Chọn font hiển thị, xem danh sách font tại https://fonts.google.com/",
    font_color: "Màu sắc chữ",
    font_highlight:
      "Hiệu ứng của chữ (Chỉ áp dụng cho tên khán giả, số tiền giao dịch và coupon)",
    font_size: "Kích thước font chữ (px), khuyến nghị từ 32-60",
    font_weight:
      "Độ đậm font chữ, lưu ý, nên tham khảo để chắc chắn font bạn đang sử dụng hỗ trợ độ đậm tương ứng. Tham khảo tại https://fonts.google.com/",
    limit: "Số lượng giao dịch",
    min_donate:
      "Số tiền nhỏ nhất để hiển thị giao dịch mới lên màn hình stream. Cấu hình thấp thôi nếu bạn là lính mới. Còn đã là cao thủ, có lượng fan hùng hậu rồi thì tăng cao lên nhé.",
    msg_template: "Mẫu lịch sử giao dịch",
    msg_template_noname:
      "Mẫu hiển thị thông báo khi khán giả chưa đăng ký trên wescan",
    msg_template_notsupport:
      "Mẫu hiển thị thông báo khi khán giả donate từ những ngân hàng không được hỗ trợ hiển thị tên bởi hệ thống wescan",
    position: "Vị trí hiển thị đầu hoặc cuối danh sách",
    show_log: "Hiển thị lịch sử giao dịch",
    sort: "Chọn giá trị để sắp xếp tăng/giảm",
    timerange: "Chọn mốc thời gian",
    timerange_type: "Chọn kiểu mốc thời gian",
  },
  qr: {
    duration: "Thời gian hiện QRCode",
    image:
      "QR Code sẽ hiển thị trên màn hình stream, bạn cũng có thể upload QR Code để thay thế",
  },
  ranking: {
    background:
      "Chọn màu nền và chọn độ mờ. Nếu muốn nền trong suốt thì kéo thanh thứ 2 sát về bên trái nhé.",
    duration: "Thời gian hiện bảng xếp hạng",
    font:
      "Chọn font hiển thị, xem danh sách font tại https://fonts.google.com/",
    font_color: "Màu sắc chữ",
    font_size: "Kích thước font chữ (px), khuyến nghị từ 18-26",
    font_weight:
      "Độ đậm font chữ, lưu ý, nên tham khảo để chắc chắn font bạn đang sử dụng hỗ trợ độ đậm tương ứng. Tham khảo tại https://fonts.google.com/",
    limit: "Số lượng hạng hiển thị trong bảng",
    row_icon: "Chọn biểu tượng hạng phù hợp với lĩnh vực mà bạn sẽ stream nhé.",
    timerange: "Chọn mốc thời gian",
    timerange_type: "Chọn kiểu mốc thời gian",
  },
  timecounter: {
    panel: {
      color: "Màu khung",
      radius: "Độ cong viền khung",
      type: "Kiểu viền khung",
    },
    text: {
      color: "Màu sắc chữ",
      font:
        "Chọn font hiển thị, xem danh sách font tại https://fonts.google.com/",
      size: "Kích thước font chữ (px), khuyến nghị từ 18-26",
      weight:
        "Độ đậm font chữ, lưu ý, nên tham khảo để chắc chắn font bạn đang sử dụng hỗ trợ độ đậm tương ứng. Tham khảo tại https://fonts.google.com/",
    },
    type:
      "Kiểu bộ đếm:\n-Mặc định: Đếm thời gian tăng dần\n-Hẹn giờ (Ngày, giờ): Chọn ngày giờ để đếm ngược.\n-Hẹn giờ (Giờ): Chọn giờ trong ngày để đếm ngược\n-Đếm ngược: Chọn thời gian để đếm ngược",
    value: "Chọn thời gian",
  },
};

export default tooltips;
